package ru.vartanyan.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vartanyan.tm.api.service.dto.*;
import ru.vartanyan.tm.configuration.ServerConfiguration;
import ru.vartanyan.tm.dto.User;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.service.TestUtil;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final IProjectService projectService = context.getBean(IProjectService.class);

    @NotNull
    private final IUserService userService = context.getBean(IUserService.class);

    @NotNull
    private final ITaskService taskService = context.getBean(ITaskService.class);

    @Before
    public void before() {
        TestUtil.initUser();
    }

    @After
    public void after() {
        userService.deleteAll();
    }

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userService.addAll(users);
        Assert.assertTrue(userService.findOneById(user1.getId()) != null);
        Assert.assertTrue(userService.findOneById(user2.getId()) != null);
        userService.remove(users.get(0));
        userService.remove(users.get(1));
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() {
        final User user = new User();
        userService.add(user);
        Assert.assertNotNull(userService.findOneById(user.getId()));
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        final int userSize = userService.findAll().size();
        userService.create("testFindAll", "test", "-");
        Assert.assertEquals(userSize + 1, userService.findAll().size());
        userService.removeByLogin("testFindAll");
    }

    @Test
    @Category(DBCategory.class)
    public void findByLogin() {
        final User user = new User();
        user.setLogin("testFindL");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        Assert.assertTrue(userService.findOneByLogin(login) != null);
        userService.removeByLogin("testFindL");
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() {
        final User user = new User();
        final String userId = user.getId();
        userService.add(user);
        Assert.assertNotNull(userService.findOneById(userId));
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() {
        final User user = new User();
        userService.add(user);
        final String userId = user.getId();
        Assert.assertTrue(userService.findOneById(userId) != null);
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void isLoginExist() {
        final User user = new User();
        user.setLogin("testExist");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void removeByLogin() {
        final User user = new User();
        user.setLogin("testRemoveByLogin");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        userService.removeByLogin(login);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTest() {
        final User user = new User();
        userService.add(user);
        final String userId = user.getId();
        userService.removeOneById(userId);
        Assert.assertFalse(userService.findOneById(userId) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() {
        final User user = new User();
        userService.add(user);
        userService.remove(user);
        Assert.assertNull(userService.findOneById(user.getId()));
    }
}
