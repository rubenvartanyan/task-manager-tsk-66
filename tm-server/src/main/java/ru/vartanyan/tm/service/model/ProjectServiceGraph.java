package ru.vartanyan.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vartanyan.tm.api.service.model.IProjectServiceGraph;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.model.ProjectGraph;
import ru.vartanyan.tm.repository.model.IProjectRepositoryGraph;
import ru.vartanyan.tm.repository.model.IUserRepositoryGraph;


import java.util.List;

@Service
public final class ProjectServiceGraph extends AbstractServiceGraph<ProjectGraph>
        implements IProjectServiceGraph {

    @NotNull
    @Autowired
    public IProjectRepositoryGraph projectRepositoryGraph;

    @NotNull
    @Autowired
    public IUserRepositoryGraph userRepositoryGraph;

    @NotNull
    public IProjectRepositoryGraph getProjectRepositoryGraph() {
            return projectRepositoryGraph;
    }

    @NotNull
    public IUserRepositoryGraph getUserRepositoryGraph() {
        return userRepositoryGraph;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final ProjectGraph entity) {
        if (entity == null) throw new NullObjectException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public ProjectGraph add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (description == null) throw new EmptyDescriptionException();
        @NotNull final ProjectGraph project = new ProjectGraph();
        project.setName(name);
        project.setDescription(description);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        project.setUser(userRepositoryGraph.findOneById(userId));
        projectRepositoryGraph.save(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<ProjectGraph> entities) {
        if (entities == null) throw new NullObjectException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        entities.forEach(projectRepositoryGraph::save);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void deleteAll() {
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(final @Nullable ProjectGraph entity) {
        if (entity == null) throw new NullObjectException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.removeOneById(entity.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public @Nullable List<ProjectGraph> findAll() {
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        return projectRepositoryGraph.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectGraph findOneById(
            @Nullable final String id
    ) {
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        if (id == null) throw new EmptyIdException();
        return projectRepositoryGraph.findOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyIdException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.removeOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null) throw new EmptyIdException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.deleteAllByUserId(userId);
    }

    @SneakyThrows
    @Override
    @Transactional
    public @NotNull List<ProjectGraph> findAll(@Nullable final String userId) {
        if (userId == null) throw new EmptyIdException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        return projectRepositoryGraph.findAllByUserId(userId);
    }


    @SneakyThrows
    @Override
    @Transactional
    public @Nullable ProjectGraph findOneByUserIdAndUserId(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        return projectRepositoryGraph.findOneByIdAndUserId(id, userId);
    }

    @SneakyThrows
    @Override
    @Transactional
    public @Nullable ProjectGraph findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        return projectRepositoryGraph.findAllByUserId(userId).get(index-1);
    }

    @SneakyThrows
    @Override
    @Transactional
    public @Nullable ProjectGraph findOneByUserIdAndName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        return projectRepositoryGraph.findOneByNameAndUserId(name, userId);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void remove(
            @Nullable final String userId,
            @Nullable final ProjectGraph entity
    ) {
        if (userId == null) throw new EmptyIdException();
        if (entity == null) throw new NullObjectException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.removeOneByIdAndUserId(userId, entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId, 
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.removeOneByIdAndUserId(userId, id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId, 
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        @NotNull ProjectGraph project = projectRepositoryGraph.findAllByUserId(userId).get(index);
        projectRepositoryGraph.removeOneByIdAndUserId(userId, project.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.removeOneByNameAndUserId(userId, name);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (status == null) throw new NullObjectException();
        @NotNull final ProjectGraph entity = findOneByUserIdAndUserId(userId, id);
        entity.setStatus(status);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final ProjectGraph entity = findOneByIndex(userId, index);
        entity.setStatus(status);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (status == null) throw new NullObjectException();
        @NotNull final ProjectGraph entity = findOneByUserIdAndName(userId, name);
        entity.setStatus(status);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final ProjectGraph entity = findOneByUserIdAndUserId(userId, id);
        entity.setStatus(Status.COMPLETE);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final ProjectGraph entity = findOneByIndex(userId, index);
        entity.setStatus(Status.COMPLETE);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final ProjectGraph entity = findOneByUserIdAndName(userId, name);
        entity.setStatus(Status.COMPLETE);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final ProjectGraph entity = findOneByUserIdAndUserId(userId, id);
        entity.setStatus(Status.IN_PROGRESS);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final ProjectGraph entity = findOneByIndex(userId, index);
        entity.setStatus(Status.IN_PROGRESS);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) return;
        @NotNull final ProjectGraph entity = findOneByUserIdAndName(userId, name);
        entity.setStatus(Status.IN_PROGRESS);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final ProjectGraph entity = findOneByUserIdAndUserId(userId, id);
        entity.setName(name);
        entity.setDescription(description);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (name == null) throw new EmptyNameException();
        @NotNull final ProjectGraph entity = findOneByIndex(userId, index);
        entity.setName(name);
        entity.setDescription(description);
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        projectRepositoryGraph.save(entity);
    }

}
