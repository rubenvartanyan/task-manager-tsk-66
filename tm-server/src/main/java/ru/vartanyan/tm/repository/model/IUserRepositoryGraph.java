package ru.vartanyan.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vartanyan.tm.api.repository.IRepositoryGraph;
import ru.vartanyan.tm.model.UserGraph;

import javax.persistence.EntityManager;
import java.util.List;

public interface IUserRepositoryGraph extends IRepositoryGraph<UserGraph> {

    @NotNull
    List<UserGraph> findAll();

    @Nullable
    UserGraph findOneById(@Nullable final String id);

    void removeOneById(@Nullable final String id);

    @Nullable
    UserGraph findByLogin(@Nullable final String login);

    void removeByLogin(@Nullable final String login);

}

