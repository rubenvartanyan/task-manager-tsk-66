package ru.vartanyan.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vartanyan.tm.api.repository.IRepository;
import ru.vartanyan.tm.dto.User;

import javax.persistence.EntityManager;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    List<User> findAll();

    @Nullable
    User findOneById(@Nullable final String id);

    void removeOneById(@Nullable final String id);

    @Nullable
    User findOneByLogin(@Nullable final String login);

    void removeByLogin(@Nullable final String login);

}
