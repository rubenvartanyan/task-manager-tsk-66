package ru.vartanyan.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class Logger implements Serializable {

    @NotNull
    protected String id = UUID.randomUUID().toString();

    @NotNull
    private Date created = new Date();

    @Nullable
    private String entity;

    @Nullable
    private String className;

    @Nullable
    private String operationType;
}
