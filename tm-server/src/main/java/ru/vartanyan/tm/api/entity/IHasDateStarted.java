package ru.vartanyan.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasDateStarted {

    @Nullable Date getDateStarted();

    void setDateStarted(@NotNull final Date dateStart);

}
