package ru.vartanyan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.vartanyan.tm.dto.AbstractEntity;

import javax.persistence.TypedQuery;

public interface IRepository<E extends AbstractEntity> extends JpaRepository<E, String> {

}
