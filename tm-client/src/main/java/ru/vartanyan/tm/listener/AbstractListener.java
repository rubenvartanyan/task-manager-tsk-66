package ru.vartanyan.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.event.ConsoleEvent;

public abstract class AbstractListener {

    @NotNull protected Bootstrap bootstrap;

    public AbstractListener() {
    }

    public void setBootstrap(@Nullable final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable public abstract String arg();

    @NotNull public abstract String name();

    @Nullable public abstract String description();

    public abstract void handler(@NotNull ConsoleEvent event) throws Exception;

}
