package ru.vartanyan.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractUserListener;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.util.TerminalUtil;

@Component
public class UserChangePasswordListener extends AbstractUserListener {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "user-change-password";
    }

    @Override
    public String description() {
        return "Change user password";
    }

    @Override
    @EventListener(condition = "@userChangePasswordListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        @Nullable final SessionDTO session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("[ENTER NEW PASSWORD]");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        adminEndpoint.setUserPassword(newPassword, session);
    }

}
