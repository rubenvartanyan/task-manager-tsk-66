package ru.vartanyan.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractTaskListener;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.util.TerminalUtil;

@Component
public class TaskRemoveByIndexListener extends AbstractTaskListener {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-remove-by-index";
    }

    @Override
    public String description() {
        return "Remove task by index";
    }

    @Override
    @EventListener(condition = "@taskRemoveByIndexListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        @Nullable final SessionDTO session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        System.out.println("[REMOVE TASK");
        System.out.println("[ENTER INDEX");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        taskEndpoint.removeTaskByIndex(index, session);
        System.out.println("[TASK REMOVED]");
    }

}
