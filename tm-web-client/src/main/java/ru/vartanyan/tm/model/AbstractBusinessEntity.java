package ru.vartanyan.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.vartanyan.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public abstract class AbstractBusinessEntity {

    protected String id = UUID.randomUUID().toString();

    protected String description = "";

    protected String name = "";

    private String userId;

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date finishDate;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

}

